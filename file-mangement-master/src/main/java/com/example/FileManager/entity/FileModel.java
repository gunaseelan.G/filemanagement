package com.example.FileManager.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name="files")
@Data
public class FileModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	private Long id;
		
		private String content_type;
	
	private String fileName;

	@Lob
	private byte[] data;

	public void setData(byte[] bytes) {
		// TODO Auto-generated method stub
		
	}

	public void setFileName(String originalFilename) {
		// TODO Auto-generated method stub
		
	}

	public void setContentType(String contentType) {
		// TODO Auto-generated method stub
		
	}
}
