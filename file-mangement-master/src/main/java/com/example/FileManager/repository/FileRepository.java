package com.example.FileManager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.FileManager.entity.FileModel;


@Repository
public interface FileRepository extends JpaRepository<FileModel,Long> {
	
	
	

}
